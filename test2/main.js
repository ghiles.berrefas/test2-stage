function init() {

    animateDiv(".a");
    animateDiv(".b");
    animateDiv(".c");

    function updateScroll() {
        var h = $("#cons")[0].scrollHeight;
        $("#cons")[0].scrollTop = h;
        // after some time, cleanup the history
        if (h >= 3000) {
            $("#cons").text("");
            updateScroll();
        }
    }

    function getRotationDegrees(obj) {
        var matrix = obj.css("transform");
        if (matrix !== 'none') {
            var values = matrix.split('(')[1].split(')')[0].split(',');
            var a = values[0];
            var b = values[1];
            var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
        } else { var angle = 0; }
        return (angle < 0) ? angle + 360 : angle;
    }

    function addRotate(zdiv, add) {
        $(zdiv).css({ 'transform': 'rotate(' + (getRotationDegrees($(zdiv)) + 1) + 'deg)' });
    }

    function makeNewPosition(zdiv) {
        // Get viewport dimensions (remove the dimension of the div)
        var h = $(window).height() - 100;
        var w = $(window).width() - 100;
        var nh = Math.floor(Math.random() * h);
        var nw = Math.floor(Math.random() * w);
        return [nh, nw];
    }

    function animateDiv(zdiv) {
        var newq = makeNewPosition(zdiv);
        var oldq = $(zdiv).offset();
        var speed = calcSpeed([oldq.top, oldq.left], newq);
        $(zdiv).animate({ top: newq[0], left: newq[1] }, {
            duration: speed,
            step: function(now, fx) {
                addRotate(zdiv, 1); /* hint... */
                const [div1, div2] = [".a", ".b", ".c"].filter(element => element !== zdiv);
                //Collision Circle 
                var collisionWithDiv1 = collision(getCenter(zdiv), getCenter(div1));
                var collisionWithDiv2 = collision(getCenter(zdiv), getCenter(div2));
                afterCollision(collisionWithDiv1, zdiv, div1, fx, oldq);
                afterCollision(collisionWithDiv2, zdiv, div2, fx, oldq);
                //Collision Edges  
                /*
                var collisionWithDiv1 = false;
                var collisionWithDiv2 = false;
                var point_edgesDiv1 = getarrayPointsEdges(div1);
                var point_edgesDiv2 = getarrayPointsEdges(div2);
                for (var i in point_edgesDiv1) {
                    if (collision2(getarrayPointsEdges(zdiv), point_edgesDiv1[i])) {
                        collisionWithDiv1 = true;
                        break;
                    }
                }
                for (var key in point_edgesDiv2) {
                    if (collision2(getarrayPointsEdges(zdiv), point_edgesDiv2[key])) {
                        collisionWithDiv2 = true;
                        break;
                    }
                }
                afterCollision(collisionWithDiv1, zdiv, div1, fx, oldq);
                afterCollision(collisionWithDiv2, zdiv, div2, fx, oldq);
                */
            },
            complete: function() { animateDiv(zdiv); }
        });
        $("#cons").append(zdiv + " going to [" + newq[0] + "," + newq[1] + "]<br/>");
        updateScroll();
    };

    function calcSpeed(prev, next) {
        var x = Math.abs(prev[1] - next[1]);
        var y = Math.abs(prev[0] - next[0]);
        var greatest = x > y ? x : y;
        var speedModifier = Math.random() / 2;
        if (speedModifier < 0.05) speedModifier = 0.05;
        var speed = Math.ceil(greatest / speedModifier);
        return speed;
    }
    //My functions
    var DELTA = 50;
    var LENGTH = 100;
    var RADIUS = (Math.sqrt(LENGTH * LENGTH + LENGTH * LENGTH) / 2);

    function getCenter(zdiv) {
        return { x: $(zdiv).position().top + DELTA, y: $(zdiv).position().left + DELTA };
    }

    function collision(circle1, circle2) {
        var dx = circle1.x - circle2.x;
        var dy = circle1.y - circle2.y;
        var distance = Math.sqrt(dx * dx + dy * dy);
        if (isNaN(distance)) return false;
        return !(distance > 2 * RADIUS);
    }

    function afterCollision(collisionWithDiv, zdiv, div, fx, oldq) {
        if (collisionWithDiv) {
            //console.log("fx.prop = " + fx.prop + " ,fx.end = " + fx.end + " ,fx.start = " + fx.start + " ,fx.now = " + fx.now);
            fx.now = fx.start;
            if (fx.prop == "top") {
                fx.end = oldq[0];
                fx.options.duration = calcSpeed($(zdiv).offset(), [fx.now, fx.end]);
                $("#cons").append("A collision was avoided between " + zdiv[1] + " and " + div[1] + "]<br/>");
            } else {
                fx.end = oldq[1];
                fx.options.duration = calcSpeed($(zdiv).offset(), [fx.now, fx.end]);
            }
        }
    }

    function collision2(tab, point_edge) {
        var i = 1;
        for (var key in tab) {
            var point_A = tab[key];
            var point_B;
            if (i == 4)
                point_B = tab["p1"];
            else
                point_B = tab["p" + (i + 1)];

            var vector_AB = { x: 0, y: 0 };
            var vector_AP = { x: 0, y: 0 };
            vector_AB.x = point_B.x - point_A.x;
            vector_AB.y = point_B.y - point_A.y;
            vector_AP.x = point_edge.x - point_A.x;
            vector_AP.y = point_edge.y - point_A.y;
            var determinant = vector_AB.x * vector_AP.y - vector_AB.y * vector_AP.x;
            i++;
            if (determinant > 0)
                return false;
        }
        return true;
    }

    function getarrayPointsEdges(zdiv) {
        return {
            p1: { x: $(zdiv).position().top, y: $(zdiv).position().left },
            p2: { x: $(zdiv).position().top, y: $(zdiv).position().left + LENGTH },
            p3: { x: $(zdiv).position().top + LENGTH, y: $(zdiv).position().left + LENGTH },
            p4: { x: $(zdiv).position().top + LENGTH, y: $(zdiv).position().left }
        }
    }

}
